﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using System.Collections.Generic;
using System.Net;

namespace Oubliette.Api.Web.Filters
{
  public class DbUpdateExceptionFilterAttribute : ExceptionFilterAttribute
  {
    private static readonly HashSet<string> _noContents = new HashSet<string>(new[]
    {
      "UQ_User_Accounts_Username"
    });

    public override void OnException(ExceptionContext context)
    {
      if (context.Exception is DbUpdateException exception
        && exception.InnerException is PostgresException postgresException
        && postgresException.ConstraintName != null)
      {
        if (_noContents.Contains(postgresException.ConstraintName))
        {
          context.ExceptionHandled = true;
          context.Result = new StatusCodeResult((int)HttpStatusCode.NoContent);
        }
      }
    }
  }
}
