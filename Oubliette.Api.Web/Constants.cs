﻿namespace Oubliette.Api.Web
{
  public static class Constants
  {
    public const string ApiTitle = "Oubliette API";
    public const string ApiVersion = "v1";
    public const string AuthorizationHeader = "Authorization";
  }
}
