using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Oubliette.Api.Application;
using Oubliette.Api.Application.Users;
using Oubliette.Api.Infrastructure;
using Oubliette.Api.Infrastructure.Mailgun;
using Oubliette.Api.Infrastructure.Users;
using Oubliette.Api.Persistence;
using Oubliette.Api.Web.Filters;
using Oubliette.Api.Web.Middlewares;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;

namespace Oubliette.Api.Web
{
  public class Startup
  {
    private const string CorsPolicy = "Oubliette Front-End";

    public static readonly HashSet<string> _publicRoutes = new HashSet<string>(new[]
    {
      "user/confirm",
      "user/login",
      "user/recover-password",
      "user/register",
      "user/renew",
      "user/reset-password"
    });

    private readonly IConfiguration _configuration;

    public Startup(IConfiguration configuration)
    {
      _configuration = configuration;
    }

    public void ConfigureServices(IServiceCollection services)
    {
      services.AddApplication();
      services.AddControllers(options =>
      {
        options.Filters.Add(new ApiExceptionFilterAttribute());
        options.Filters.Add(new DbUpdateExceptionFilterAttribute());
        options.Filters.Add(new ModelValidationFilterAttribute());
      });
      services.AddCors(options => options.AddPolicy(CorsPolicy, policy => policy
        .SetIsOriginAllowed(IsOriginAllowed)
        .WithOrigins("1", "2") // https://fetch.spec.whatwg.org/#cors-protocol-and-http-caches
        .AllowAnyMethod()
        .AllowAnyHeader()
        .AllowCredentials()
      ));
      services.AddHttpContextAccessor();
      services.AddInfrastructure();
      services.AddOpenApi();
      services.AddPersistence();

      services.Configure<TokenOptions>(_configuration.GetSection(nameof(TokenOptions)));
      services.Configure<HttpOptions>(options => options.Domain = _configuration.GetValue<string>("HTTP_DOMAIN"));
      services.Configure<JwtOptions>(options => options.Secret = _configuration.GetValue<string>("JWT_SECRET"));
      services.Configure<MailgunOptions>(options =>
      {
        options.ApiKey = _configuration.GetValue<string>("MAILGUN_API_KEY");
        options.Endpoint = _configuration.GetValue<string>("MAILGUN_ENDPOINT");
        options.Sender = _configuration.GetValue<string>("MAILGUN_SENDER");
      });

      services.AddSingleton<IApplicationContext, HttpApplicationContext>();

      JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
    }

    public void Configure(IApplicationBuilder application, IWebHostEnvironment environment)
    {
      if (environment.IsDevelopment())
      {
        application.UseDeveloperExceptionPage();
        application.UseSwagger();
        application.UseSwaggerUI(config => config.SwaggerEndpoint(
          "/swagger/v1/swagger.json",
          $"{Constants.ApiTitle} {Constants.ApiVersion}"
        ));
      }

      application.UseHttpsRedirection();
      application.UseRouting();
      application.UseCors(CorsPolicy);
      application.UseWhen(RequiresAuth, application => application.UseMiddleware<Authentication>());
      application.UseAuthorization();
      application.UseEndpoints(endpoints => endpoints.MapControllers());
    }

    private bool IsOriginAllowed(string origin)
    {
      var uri = new Uri(origin);
      return uri.Host == "localhost" || uri.Host.EndsWith(".oubliette.xyz");
    }

    private bool RequiresAuth(HttpContext context)
    {
      PathString path = context.Request.Path;

      if (path.HasValue)
      {
        string trimmed = path.Value.Trim('/');
        return !string.IsNullOrEmpty(trimmed) && !_publicRoutes.Contains(trimmed);
      }

      return false;
    }
  }
}
