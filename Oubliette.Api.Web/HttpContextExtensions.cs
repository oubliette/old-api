﻿using Microsoft.AspNetCore.Http;
using Oubliette.Domain.Users;
using System.Collections.Generic;

namespace Oubliette.Api.Web
{
  public static class HttpContextExtensions
  {
    private const string AccountKey = nameof(Account);

    public static Account GetAccount(this HttpContext context)
    {
      if (context.Items.TryGetValue(AccountKey, out object value))
      {
        return (Account)value;
      }

      return null;
    }

    public static bool SetAccount(this HttpContext context, Account account)
    {
      if (context.Items.ContainsKey(AccountKey))
      {
        if (!context.Items.Remove(AccountKey))
        {
          return false;
        }
      }

      return account == null || context.Items.TryAdd(AccountKey, account);
    }
  }
}
