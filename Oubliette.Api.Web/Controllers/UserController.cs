﻿#nullable enable
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Oubliette.Api.Application.Users.Commands;
using Oubliette.Api.Application.Users.Models;
using Oubliette.Api.Application.Users.Queries;
using System.Threading;
using System.Threading.Tasks;

namespace Oubliette.Api.Web.Controllers
{
  [Route("user")]
  public class UserController : Controller
  {
    private readonly IMediator _mediator;

    public UserController(IMediator mediator)
    {
      _mediator = mediator;
    }

    [HttpPost("change-password")]
    public async Task<ActionResult> ChangePasswordAsync([FromBody] ChangePasswordModel model, CancellationToken cancellationToken)
    {
      await _mediator.Send(new ChangePassword(model), cancellationToken);
      return NoContent();
    }

    [HttpPost("confirm")]
    public async Task<ActionResult> ConfirmAsync([FromBody] ConfirmModel model, CancellationToken cancellationToken)
    {
      await _mediator.Send(new Confirm(model), cancellationToken);
      return NoContent();
    }

    [HttpPost("login")]
    public async Task<ActionResult<TokenModel>> LogInAsync([FromBody] LogInModel model, CancellationToken cancellationToken)
    {
      return Ok(await _mediator.Send(new LogIn(model), cancellationToken));
    }

    [HttpPost("logout")]
    public async Task<ActionResult> LogOutAsync([FromBody] LogOutModel model, CancellationToken cancellationToken)
    {
      await _mediator.Send(new LogOut(model), cancellationToken);
      return NoContent();
    }

    [HttpGet("profile")]
    public async Task<ActionResult<ProfileModel>> GetProfileAsync(CancellationToken cancellationToken)
    {
      return Ok(await _mediator.Send(new GetProfile(), cancellationToken));
    }

    [HttpPost("recover-password")]
    public async Task<ActionResult> RegisterAsync([FromBody] RecoverPasswordModel model, CancellationToken cancellationToken)
    {
      await _mediator.Send(new RecoverPassword(model), cancellationToken);
      return NoContent();
    }

    [HttpPost("register")]
    public async Task<ActionResult> RegisterAsync([FromBody] RegisterModel model, CancellationToken cancellationToken)
    {
      await _mediator.Send(new Register(model), cancellationToken);
      return NoContent();
    }

    [HttpPost("renew")]
    public async Task<ActionResult<TokenModel>> RenewAsync([FromBody] RenewModel model, CancellationToken cancellationToken)
    {
      return Ok(await _mediator.Send(new Renew(model), cancellationToken));
    }

    [HttpPost("reset-password")]
    public async Task<ActionResult> ResetPasswordAsync([FromBody] ResetPasswordModel model, CancellationToken cancellationToken)
    {
      await _mediator.Send(new ResetPassword(model), cancellationToken);
      return NoContent();
    }
  }
}
