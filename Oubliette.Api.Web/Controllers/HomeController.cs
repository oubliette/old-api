﻿#nullable enable
using Microsoft.AspNetCore.Mvc;

namespace Oubliette.Api.Web.Controllers
{
  [Route("")]
  public class HomeController : Controller
  {
    [HttpGet]
    public IActionResult Index()
    {
      return Ok($"{Constants.ApiTitle} {Constants.ApiVersion}");
    }
  }
}
