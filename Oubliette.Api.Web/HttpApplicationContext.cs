﻿using Microsoft.AspNetCore.Http;
using Oubliette.Api.Application;
using Oubliette.Domain.Users;

namespace Oubliette.Api.Web
{
  public class HttpApplicationContext : IApplicationContext
  {
    private readonly IHttpContextAccessor _httpContextAccessor;

    public HttpApplicationContext(IHttpContextAccessor httpContextAccessor)
    {
      _httpContextAccessor = httpContextAccessor;
    }

    public Account Account => _httpContextAccessor.HttpContext.GetAccount();
  }
}
