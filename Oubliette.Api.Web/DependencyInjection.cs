﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;

namespace Oubliette.Api.Web
{
  public static class DependencyInjection
  {
    public static IServiceCollection AddOpenApi(this IServiceCollection services)
    {
      return services.AddSwaggerGen(config =>
      {
        config.AddSecurityDefinition("Authorization", new OpenApiSecurityScheme
        {
          Description = "Bearer {token}",
          In = ParameterLocation.Header,
          Name = Constants.AuthorizationHeader,
          Type = SecuritySchemeType.ApiKey
        });
        config.AddSecurityRequirement(new OpenApiSecurityRequirement
        {
          {
            new OpenApiSecurityScheme
            {
              Description = "Bearer {token}",
              In = ParameterLocation.Header,
              Name = Constants.AuthorizationHeader,
              Reference = new OpenApiReference
              {
                Id = "Authorization",
                Type = ReferenceType.SecurityScheme
              },
              Type = SecuritySchemeType.ApiKey
            },
            Array.Empty<string>()
          }
        });

        config.SwaggerDoc(Constants.ApiVersion, new OpenApiInfo
        {
          Contact = new OpenApiContact
          {
            Email = "francispion@hotmail.com",
            Name = "Francis Pion",
            Url = new Uri("https://app.oubliette.xyz")
          },
          Description = "d20 SRD 5e management Web API.",
          License = new OpenApiLicense
          {
            Name = "Use under MIT",
            Url = new Uri("https://gitlab.com/oubliette/api/-/blob/master/LICENSE")
          },
          Title = Constants.ApiTitle,
          Version = Constants.ApiVersion
        });
      });
    }
  }
}
