﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Oubliette.Api.Application;
using Oubliette.Api.Application.Users;
using Oubliette.Domain.Users;
using System;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Oubliette.Api.Web.Middlewares
{
  public class Authentication
  {
    private readonly RequestDelegate _next;

    public Authentication(RequestDelegate next)
    {
      _next = next;
    }

    public async Task InvokeAsync(
      HttpContext httpContext,
      IOublietteContext dbContext,
      IToken token,
      IOptions<TokenOptions> tokenOptions
    )
    {
      HttpRequest request = httpContext.Request;
      HttpResponse response = httpContext.Response;

      string type = tokenOptions.Value.Type;

      if (!request.Headers.TryGetValue(Constants.AuthorizationHeader, out StringValues values))
      {
        await SetResponseAsync(response, "missing_auth", type);
        return;
      }

      string value = values.Single();
      string[] parts = value.Split();
      if (parts.Length != 2)
      {
        await SetResponseAsync(response, "invalid_auth", type);
        return;
      }
      else if (parts[0].ToLowerInvariant() != tokenOptions.Value.Type.ToLowerInvariant())
      {
        await SetResponseAsync(response, "not_supported", type);
        return;
      }

      ClaimsPrincipal principal;
      try
      {
        principal = token.Validate(parts[1]);
      }
      catch (Exception)
      {
        await SetResponseAsync(response, "invalid_credentials", type);
        return;
      }

      Claim claim = principal.Claims.Single(x => x.Type == "sub");
      var uuid = Guid.Parse(claim.Value);

      Account account = await dbContext.Accounts.SingleAsync(x => x.Uuid == uuid);

      if (!httpContext.SetAccount(account))
      {
        throw new InvalidOperationException("The Account context item could not be set.");
      }

      await _next(httpContext);
    }

    private static async Task SetResponseAsync(HttpResponse response, string code, string type)
    {
      response.ContentType = MediaTypeNames.Application.Json;
      response.Headers.Add("WWW-Authenticate", type);
      response.StatusCode = (int)HttpStatusCode.Unauthorized;

      await response.WriteAsJsonAsync(new { code });
    }
  }
}
