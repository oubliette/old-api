--- SECTION UTILISATEURS ---
CREATE SCHEMA "User";

CREATE TABLE "User"."Accounts"
(
  "AccountId" SERIAL NOT NULL,
  "AccountUuid" UUID NOT NULL,
  "Confirmed" TIMESTAMP NULL,
  "Created" TIMESTAMP NOT NULL,
  "Email" VARCHAR(255) NOT NULL,
  "Name" VARCHAR(255) NOT NULL,
  "PasswordBytes" BYTEA NOT NULL,
  "PasswordChanged" TIMESTAMP NULL,
  "Updated" TIMESTAMP NULL,
  "Username" VARCHAR(255) NOT NULL,
  CONSTRAINT "PK_User_Accounts" PRIMARY KEY ("AccountId"),
  CONSTRAINT "UQ_User_Accounts_AccountUuid" UNIQUE ("AccountUuid"),
  CONSTRAINT "UQ_User_Accounts_Username" UNIQUE ("Username")
);

CREATE TABLE "User"."PasswordResets"
(
  "AccountId" INT NOT NULL,
  "Created" TIMESTAMP NOT NULL,
  "PasswordResetUuid" UUID NOT NULL,
  "Updated" TIMESTAMP NULL,
  CONSTRAINT "PK_User_PasswordResets" PRIMARY KEY ("AccountId"),
  CONSTRAINT "UQ_User_PasswordResets_PasswordResetUuid" UNIQUE ("PasswordResetUuid"),
  CONSTRAINT "FK_User_PasswordResets_AccountId" FOREIGN KEY ("AccountId") REFERENCES "User"."Accounts" ("AccountId") ON DELETE CASCADE
);

CREATE TABLE "User"."Sessions"
(
  "AccountId" INT NOT NULL,
  "Created" TIMESTAMP NOT NULL,
  "KeyBytes" BYTEA NOT NULL,
  "SessionId" SERIAL NOT NULL,
  "SessionUuid" UUID NOT NULL,
  "Updated" TIMESTAMP NULL,
  CONSTRAINT "PK_User_Sessions" PRIMARY KEY ("SessionId"),
  CONSTRAINT "UQ_User_Sessions_SessionUuid" UNIQUE ("SessionUuid"),
  CONSTRAINT "FK_User_Sessions_AccountId" FOREIGN KEY ("AccountId") REFERENCES "User"."Accounts" ("AccountId") ON DELETE CASCADE
);
