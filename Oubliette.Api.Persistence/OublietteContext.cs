﻿using Microsoft.EntityFrameworkCore;
using Oubliette.Api.Application;
using Oubliette.Domain.Users;

namespace Oubliette.Api.Persistence
{
  public class OublietteContext : DbContext, IOublietteContext
  {
    public OublietteContext(DbContextOptions<OublietteContext> options) : base(options)
    {
    }

    public DbSet<Account> Accounts { get; set; }
    public DbSet<PasswordReset> PasswordResets { get; set; }
    public DbSet<Session> Sessions { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);
      modelBuilder.Ignore<Password>();
    }
  }
}
