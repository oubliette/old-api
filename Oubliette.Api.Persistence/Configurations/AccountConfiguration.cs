﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Oubliette.Domain.Users;

namespace Oubliette.Api.Persistence.Configurations
{
  public class AccountConfiguration : IEntityTypeConfiguration<Account>
  {
    public void Configure(EntityTypeBuilder<Account> builder)
    {
      builder.ToTable("Accounts", Schemas.User);
      builder.HasKey(x => x.Id);
      builder.Property(x => x.Id).HasColumnName("AccountId");
      builder.Property(x => x.Uuid).HasColumnName("AccountUuid");
    }
  }
}
