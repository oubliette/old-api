﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Oubliette.Domain.Users;

namespace Oubliette.Api.Persistence.Configurations
{
  public class SessionConfiguration : IEntityTypeConfiguration<Session>
  {
    public void Configure(EntityTypeBuilder<Session> builder)
    {
      builder.ToTable("Sessions", Schemas.User);
      builder.HasKey(x => x.Id);
      builder.Property(x => x.Id).HasColumnName("SessionId");
      builder.Property(x => x.Uuid).HasColumnName("SessionUuid");
    }
  }
}
