﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Oubliette.Domain.Users;

namespace Oubliette.Api.Persistence.Configurations
{
  public class PasswordResetConfiguration : IEntityTypeConfiguration<PasswordReset>
  {
    public void Configure(EntityTypeBuilder<PasswordReset> builder)
    {
      builder.ToTable("PasswordResets", Schemas.User);
      builder.HasKey(x => x.AccountId);
      builder.Property(x => x.Uuid).HasColumnName("PasswordResetUuid");
    }
  }
}
