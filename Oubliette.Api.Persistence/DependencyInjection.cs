﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Oubliette.Api.Application;
using System;

namespace Oubliette.Api.Persistence
{
  public static class DependencyInjection
  {
    public static IServiceCollection AddPersistence(this IServiceCollection services)
    {
      return services.AddDbContext<IOublietteContext, OublietteContext>((provider, options) =>
      {
        string connectionString;

        string databaseUrl = Environment.GetEnvironmentVariable("DATABASE_URL");
        if (string.IsNullOrEmpty(databaseUrl))
        {
          var configuration = provider.GetRequiredService<IConfiguration>();
          connectionString = configuration.GetConnectionString(nameof(OublietteContext));
        }
        else
        {
          var uri = new Uri(databaseUrl);
          string[] credentials = uri.UserInfo.Split(':');

          connectionString = string.Format(
            "User ID={0};Password={1};Host={2};Port={3};Database={4};SSL Mode=Require;Trust Server Certificate=True;",
            credentials[0],
            credentials[1],
            uri.Host,
            uri.Port,
            uri.LocalPath.TrimStart('/')
          );
        }

        options.UseNpgsql(connectionString);
      });
    }
  }
}
