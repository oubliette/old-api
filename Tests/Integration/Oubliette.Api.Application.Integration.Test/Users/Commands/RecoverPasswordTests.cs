﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Moq;
using Oubliette.Api.Application.Users.Models;
using Oubliette.Api.Persistence;
using Oubliette.Domain.Email;
using Oubliette.Domain.Users;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Oubliette.Api.Application.Users.Commands
{
  [Trait(Traits.Category, Categories.Integration)]
  public class RecoverPasswordTests
  {
    private readonly Mock<IDateTime> _dateTime = new Mock<IDateTime>();
    private readonly Mock<IEmail> _email = new Mock<IEmail>();
    private readonly Mock<IGuid> _guid = new Mock<IGuid>();
    private readonly Mock<IToken> _token = new Mock<IToken>();

    private readonly OublietteContext _dbContext;
    private readonly RecoverPasswordHandler _handler;
    private readonly HttpOptions _httpOptions = new HttpOptions
    {
      Domain = "domain"
    };

    public RecoverPasswordTests()
    {
      _dbContext = new OublietteContext(new DbContextOptionsBuilder<OublietteContext>()
       .UseInMemoryDatabase(nameof(RecoverPasswordTests))
       .Options);

      _dbContext.Database.EnsureDeleted();
      _dbContext.Database.EnsureCreated();

      _handler = new RecoverPasswordHandler(
        _dateTime.Object,
        _dbContext,
        _email.Object,
        _guid.Object,
        Options.Create(_httpOptions),
        _token.Object
      );
    }

    [Fact]
    public async Task Given_NoMatchingUsername_When_Handle_Then_NoPasswordReset()
    {
      await _handler.Handle(new RecoverPassword(new RecoverPasswordModel
      {
        Username = string.Empty
      }), default);
      Assert.Empty(_dbContext.PasswordResets.ToArray());
    }

    [Fact]
    public async Task Given_NullPasswordReset_When_Handle_Then_Created()
    {
      CancellationToken cancellationToken = default;

      var account = new Account
      {
        Username = "test"
      };
      _dbContext.Accounts.Add(account);
      _dbContext.SaveChanges();

      var created = DateTime.Now;
      _dateTime.SetupGet(x => x.Now).Returns(created);

      var uuid = Guid.NewGuid();
      _guid.Setup(x => x.NewGuid()).Returns(uuid);

      string token = "token";
      _token.Setup(x => x.Create(It.Is<ClaimsIdentity>(y
        => y.Claims.Single(x => x.Type == "sub").Value == uuid.ToString()
      ), 86400)).Returns(token);

      await _handler.Handle(new RecoverPassword(new RecoverPasswordModel
      {
        Username = account.Username
      }), cancellationToken);

      PasswordReset passwordReset = _dbContext.PasswordResets.Single();
      Assert.Equal(account.Id, passwordReset.AccountId);
      Assert.Equal(created, passwordReset.Created);
      Assert.Equal(uuid, passwordReset.Uuid);
      Assert.Null(passwordReset.Updated);

      _email.Verify(x => x.SendAsync(It.Is<Message>(y
        => y.Body.Contains(_httpOptions.Domain)
        && y.Body.Contains(account.Name)
        && y.Body.Contains(token)
        && y.Recipients.Single().Address == account.Email
        && y.Recipients.Single().DisplayName == account.Name
        && y.Recipients.Single().Type == RecipientType.To
      ), cancellationToken));
    }

    [Fact]
    public async Task Given_PasswordReset_When_Handle_Then_Updated()
    {
      CancellationToken cancellationToken = default;

      var created = DateTime.Now;
      var account = new Account
      {
        PasswordReset = new PasswordReset
        {
          Created = created
        },
        Username = "test"
      };
      _dbContext.Accounts.Add(account);
      _dbContext.SaveChanges();

      DateTime updated = created.AddHours(1);
      _dateTime.SetupGet(x => x.Now).Returns(updated);

      var uuid = Guid.NewGuid();
      _guid.Setup(x => x.NewGuid()).Returns(uuid);

      string token = "token";
      _token.Setup(x => x.Create(It.Is<ClaimsIdentity>(y
        => y.Claims.Single(x => x.Type == "sub").Value == uuid.ToString()
      ), 86400)).Returns(token);

      await _handler.Handle(new RecoverPassword(new RecoverPasswordModel
      {
        Username = account.Username
      }), cancellationToken);

      PasswordReset passwordReset = _dbContext.PasswordResets.Single();
      Assert.Equal(account.Id, passwordReset.AccountId);
      Assert.Equal(created, passwordReset.Created);
      Assert.Equal(updated, passwordReset.Updated);
      Assert.Equal(uuid, passwordReset.Uuid);

      _email.Verify(x => x.SendAsync(It.Is<Message>(y
        => y.Body.Contains(_httpOptions.Domain)
        && y.Body.Contains(account.Name)
        && y.Body.Contains(token)
        && y.Recipients.Single().Address == account.Email
        && y.Recipients.Single().DisplayName == account.Name
        && y.Recipients.Single().Type == RecipientType.To
      ), cancellationToken));
    }
  }
}
