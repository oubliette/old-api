﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Moq;
using Oubliette.Api.Application.Users.Models;
using Oubliette.Api.Persistence;
using Oubliette.Domain.Email;
using Oubliette.Domain.Users;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Oubliette.Api.Application.Users.Commands
{
  [Trait(Traits.Category, Categories.Integration)]
  public class RegisterTests
  {
    private readonly Mock<IDateTime> _dateTime = new Mock<IDateTime>();
    private readonly Mock<IEmail> _email = new Mock<IEmail>();
    private readonly Mock<IGuid> _guid = new Mock<IGuid>();
    private readonly Mock<IPassword> _password = new Mock<IPassword>();
    private readonly Mock<IToken> _token = new Mock<IToken>();

    private readonly OublietteContext _dbContext;
    private readonly RegisterHandler _handler;
    private readonly HttpOptions _httpOptions = new HttpOptions
    {
      Domain = "domain"
    };

    public RegisterTests()
    {
      _dbContext = new OublietteContext(new DbContextOptionsBuilder<OublietteContext>()
        .UseInMemoryDatabase(nameof(RegisterTests))
        .Options);

      _dbContext.Database.EnsureDeleted();
      _dbContext.Database.EnsureCreated();

      _handler = new RegisterHandler(
        _dateTime.Object,
        _dbContext,
        _email.Object,
        _guid.Object,
        Options.Create(_httpOptions),
        _password.Object,
        _token.Object
      );
    }

    [Fact]
    public async Task Given_Request_When_Handle_Then_Registered()
    {
      CancellationToken cancellationToken = default;

      var model = new RegisterModel
      {
        Email = "info@TEST.com",
        Name = " Test User   ",
        Password = "Test123!"
      };

      var created = DateTime.Now;
      _dateTime.SetupGet(x => x.Now).Returns(created);

      var uuid = Guid.NewGuid();
      _guid.Setup(x => x.NewGuid()).Returns(uuid);

      var password = new Password(model.Password, new byte[32], 10000);
      _password.Setup(x => x.Create(model.Password)).Returns(password);

      string token = "token";
      _token.Setup(x => x.Create(It.Is<ClaimsIdentity>(y
        => y.Claims.Single(x => x.Type == "sub").Value == uuid.ToString()
      ), 864000)).Returns(token);

      await _handler.Handle(new Register(model), cancellationToken);

      Account account = _dbContext.Accounts.Single();
      Assert.Equal(created, account.Created);
      Assert.Equal(uuid, account.Uuid);
      Assert.Equal(model.Email, account.Email);
      Assert.Equal(model.Name.CleanTrim(), account.Name);
      Assert.Equal(model.Email.ToLowerInvariant(), account.Username);

      Assert.Same(password, account.Password);
      Assert.True(account.Password.Match(model.Password));

      _email.Verify(x => x.SendAsync(It.Is<Message>(y
        => y.Body.Contains(_httpOptions.Domain)
        && y.Body.Contains(account.Name)
        && y.Body.Contains(token)
        && y.Recipients.Single().Address == account.Email
        && y.Recipients.Single().DisplayName == account.Name
        && y.Recipients.Single().Type == RecipientType.To
      ), cancellationToken));
    }
  }
}
