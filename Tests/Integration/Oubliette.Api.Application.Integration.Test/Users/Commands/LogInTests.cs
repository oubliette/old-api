﻿using Microsoft.EntityFrameworkCore;
using Moq;
using Oubliette.Api.Application.Exceptions;
using Oubliette.Api.Application.Users.Models;
using Oubliette.Api.Persistence;
using Oubliette.Domain.Users;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Oubliette.Api.Application.Users.Commands
{
  [Trait(Traits.Category, Categories.Integration)]
  public class LogInTests
  {
    private const string PasswordString = "Test123!";

    private readonly Mock<IAuthentication> _authentication = new Mock<IAuthentication>();
    private readonly Mock<IDateTime> _dateTime = new Mock<IDateTime>();
    private readonly Mock<IGuid> _guid = new Mock<IGuid>();

    private readonly Account _account;
    private readonly OublietteContext _dbContext;
    private readonly LogInHandler _handler;

    public LogInTests()
    {
      _dbContext = new OublietteContext(new DbContextOptionsBuilder<OublietteContext>()
        .UseInMemoryDatabase(nameof(LogInTests))
        .Options);

      _dbContext.Database.EnsureDeleted();
      _dbContext.Database.EnsureCreated();

      _account = new Account
      {
        Confirmed = DateTime.Now,
        Password = new Password(PasswordString, new byte[32], 10000),
        Username = "test"
      };
      _dbContext.Accounts.Add(_account);
      _dbContext.SaveChanges();

      _handler = new LogInHandler(
        _authentication.Object,
        _dateTime.Object,
        _dbContext,
        _guid.Object
      );
    }

    [Fact]
    public async Task Given_InvalidPassword_When_Handle_Then_InvalidCredentialException()
    {
      await Assert.ThrowsAsync<InvalidCredentialException>(async ()
        => await _handler.Handle(new LogIn(new LogInModel
        {
          Password = PasswordString + "*",
          Username = _account.Username
        }), default));
    }

    [Fact]
    public async Task Given_NoMatchingUsername_When_Handle_Then_InvalidCredentialException()
    {
      await Assert.ThrowsAsync<InvalidCredentialException>(async ()
        => await _handler.Handle(new LogIn(new LogInModel
        {
          Username = _account.Username + "*"
        }), default));
    }

    [Fact]
    public async Task Given_NotConfirmed_When_Handle_Then_NotConfirmedException()
    {
      _account.Confirmed = null;
      _dbContext.SaveChanges();

      await Assert.ThrowsAsync<NotConfirmedException>(async ()
        => await _handler.Handle(new LogIn(new LogInModel
        {
          Password = PasswordString,
          Username = _account.Username
        }), default));
    }

    [Fact]
    public async Task Given_ValidCredentials_When_Handle_Then_LoggedIn()
    {
      var created = DateTime.Now;
      _dateTime.SetupGet(x => x.Now).Returns(created);

      var uuid = Guid.NewGuid();
      _guid.Setup(x => x.NewGuid()).Returns(uuid);

      var model = new TokenModel();
      _authentication.Setup(x => x.CreateToken(It.Is<Session>(y
        => y.Account != null
        && y.AccountId == _account.Id
        && y.Created == created
        && y.Uuid == uuid
      ))).Returns(model);

      TokenModel result = await _handler.Handle(new LogIn(new LogInModel
      {
        Password = PasswordString,
        Username = _account.Username
      }), default);
      Assert.Same(model, result);
    }
  }
}
