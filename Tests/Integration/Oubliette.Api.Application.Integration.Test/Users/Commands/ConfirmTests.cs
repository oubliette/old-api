﻿using Microsoft.EntityFrameworkCore;
using Moq;
using Oubliette.Api.Application.Users.Models;
using Oubliette.Api.Persistence;
using Oubliette.Domain.Users;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace Oubliette.Api.Application.Users.Commands
{
  [Trait(Traits.Category, Categories.Integration)]
  public class ConfirmTests
  {
    private readonly Mock<IDateTime> _dateTime = new Mock<IDateTime>();
    private readonly Mock<IToken> _token = new Mock<IToken>();

    private readonly Account _account;
    private readonly OublietteContext _dbContext;
    private readonly ConfirmHandler _handler;

    public ConfirmTests()
    {
      _dbContext = new OublietteContext(new DbContextOptionsBuilder<OublietteContext>()
        .UseInMemoryDatabase(nameof(ConfirmTests))
        .Options);

      _dbContext.Database.EnsureDeleted();
      _dbContext.Database.EnsureCreated();

      _account = new Account
      {
        Uuid = Guid.NewGuid()
      };
      _dbContext.Accounts.Add(_account);
      _dbContext.SaveChanges();

      _handler = new ConfirmHandler(
        _dateTime.Object,
        _dbContext,
        _token.Object
      );
    }

    [Fact]
    public async Task Given_AlreadyConfirmed_When_Handle_Then_NotUpdated()
    {
      var confirmed = DateTime.Now.AddDays(-1);
      _account.Confirmed = confirmed;
      _dbContext.SaveChanges();

      var model = new ConfirmModel
      {
        Token = "token"
      };

      _token.Setup(x => x.Validate(model.Token)).Returns(new ClaimsPrincipal(new ClaimsIdentity(new[]
      {
        new Claim("sub", _account.Uuid.ToString())
      })));

      await _handler.Handle(new Confirm(model), default);

      Assert.Equal(confirmed, _account.Confirmed);
    }

    [Fact]
    public async Task Given_ValidToken_When_Handle_Then_Confirm()
    {
      var model = new ConfirmModel
      {
        Token = "token"
      };

      var confirmed = DateTime.Now;
      _dateTime.SetupGet(x => x.Now).Returns(confirmed);

      _token.Setup(x => x.Validate(model.Token)).Returns(new ClaimsPrincipal(new ClaimsIdentity(new[]
      {
        new Claim("sub", _account.Uuid.ToString())
      })));

      await _handler.Handle(new Confirm(model), default);

      Assert.Equal(confirmed, _account.Confirmed);
    }
  }
}
