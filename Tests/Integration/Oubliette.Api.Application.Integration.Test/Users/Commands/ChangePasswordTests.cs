﻿using Microsoft.EntityFrameworkCore;
using Moq;
using Oubliette.Api.Application.Exceptions;
using Oubliette.Api.Application.Users.Models;
using Oubliette.Api.Persistence;
using Oubliette.Domain.Users;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Oubliette.Api.Application.Users.Commands
{
  [Trait(Traits.Category, Categories.Integration)]
  public class ChangePasswordTests
  {
    private const string PasswordString = "Test123!";

    private readonly Mock<IApplicationContext> _appContext = new Mock<IApplicationContext>();
    private readonly Mock<IDateTime> _dateTime = new Mock<IDateTime>();
    private readonly Mock<IPassword> _password = new Mock<IPassword>();

    private readonly Account _account;
    private readonly OublietteContext _dbContext;
    private readonly ChangePasswordHandler _handler;

    public ChangePasswordTests()
    {
      _dbContext = new OublietteContext(new DbContextOptionsBuilder<OublietteContext>()
        .UseInMemoryDatabase(nameof(ChangePasswordTests))
        .Options);

      _dbContext.Database.EnsureDeleted();
      _dbContext.Database.EnsureCreated();

      _account = new Account
      {
        Password = new Password(PasswordString, new byte[32], 10000)
      };
      _dbContext.Accounts.Add(_account);
      _dbContext.SaveChanges();

      _handler = new ChangePasswordHandler(
        _appContext.Object,
        _dateTime.Object,
        _dbContext,
        _password.Object
      );
    }

    [Fact]
    public async Task Given_InvalidCurrent_When_Handle_Then_InvalidCredentialException()
    {
      _appContext.SetupGet(x => x.Account).Returns(_account);

      await Assert.ThrowsAsync<InvalidCredentialException>(async ()
        => await _handler.Handle(new ChangePassword(new ChangePasswordModel
        {
          Current = PasswordString + "*"
        }), default));
    }

    [Fact]
    public async Task Given_ValidCurrent_When_Handle_Then_InvalidCredentialException()
    {
      string newPassword = "P@s$W0rD";

      _appContext.SetupGet(x => x.Account).Returns(_account);

      var password = new Password(newPassword, new byte[32], 10000);
      _password.Setup(x => x.Create(newPassword)).Returns(password);

      var passwordChanged = DateTime.Now;
      _dateTime.SetupGet(x => x.Now).Returns(passwordChanged);

      await _handler.Handle(new ChangePassword(new ChangePasswordModel
      {
        Current = PasswordString,
        Password = newPassword
      }), default);

      Assert.Equal(passwordChanged, _account.PasswordChanged);
      Assert.Same(password, _account.Password);
    }
  }
}
