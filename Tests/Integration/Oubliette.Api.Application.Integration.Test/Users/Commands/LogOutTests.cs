﻿using Microsoft.EntityFrameworkCore;
using Oubliette.Api.Application.Exceptions;
using Oubliette.Api.Application.Users.Models;
using Oubliette.Api.Persistence;
using Oubliette.Domain.Users;
using System;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Xunit;

namespace Oubliette.Api.Application.Users.Commands
{
  [Trait(Traits.Category, Categories.Integration)]
  public class LogOutTests
  {
    private readonly OublietteContext _dbContext;
    private readonly LogOutHandler _handler;
    private readonly byte[] _keyBytes = new byte[32];
    private readonly Session _session;

    public LogOutTests()
    {
      _dbContext = new OublietteContext(new DbContextOptionsBuilder<OublietteContext>()
       .UseInMemoryDatabase(nameof(LogOutTests))
       .Options);

      _dbContext.Database.EnsureDeleted();
      _dbContext.Database.EnsureCreated();

      using var generator = new RNGCryptoServiceProvider();
      generator.GetBytes(_keyBytes);

      _session = new Session
      {
        Key = new Password(Convert.ToBase64String(_keyBytes), new byte[32], 10000),
        Uuid = Guid.NewGuid()
      };
      _dbContext.Sessions.Add(_session);
      _dbContext.SaveChanges();

      _handler = new LogOutHandler(_dbContext);
    }

    [Fact]
    public async Task Given_InvalidKey_When_Handle_Then_InvalidCredentialException()
    {
      await Assert.ThrowsAsync<InvalidCredentialException>(async ()
        => await _handler.Handle(new LogOut(new LogOutModel
        {
          RefreshToken = new RefreshToken(_session.Uuid, new byte[_keyBytes.Length]).ToString()
        }), default));
    }

    [Fact]
    public async Task Given_InvalidRefreshToken_When_Handle_Then_InvalidCredentialException()
    {
      await Assert.ThrowsAsync<InvalidCredentialException>(async ()
        => await _handler.Handle(new LogOut(new LogOutModel
        {
          RefreshToken = "token"
        }), default));
    }

    [Fact]
    public async Task Given_NoMatchingSession_When_Handle_Then_InvalidCredentialException()
    {
      await Assert.ThrowsAsync<InvalidCredentialException>(async ()
        => await _handler.Handle(new LogOut(new LogOutModel
        {
          RefreshToken = new RefreshToken(Guid.Empty, _keyBytes).ToString()
        }), default));
    }

    [Fact]
    public async Task Given_RefreshToken_When_Handle_Then_TokenModel()
    {
      await _handler.Handle(new LogOut(new LogOutModel
      {
        RefreshToken = new RefreshToken(_session.Uuid, _keyBytes).ToString()
      }), default);

      Assert.Null(_dbContext.Sessions.Find(_session.Id));
    }
  }
}
