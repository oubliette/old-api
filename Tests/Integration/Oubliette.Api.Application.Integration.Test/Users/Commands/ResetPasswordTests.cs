﻿using Microsoft.EntityFrameworkCore;
using Moq;
using Oubliette.Api.Application.Exceptions;
using Oubliette.Api.Application.Users.Models;
using Oubliette.Api.Persistence;
using Oubliette.Domain.Users;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace Oubliette.Api.Application.Users.Commands
{
  [Trait(Traits.Category, Categories.Integration)]
  public class ResetPasswordTests
  {
    private readonly Mock<IDateTime> _dateTime = new Mock<IDateTime>();
    private readonly Mock<IPassword> _password = new Mock<IPassword>();
    private readonly Mock<IToken> _token = new Mock<IToken>();

    private readonly OublietteContext _dbContext;
    private readonly ResetPasswordHandler _handler;

    public ResetPasswordTests()
    {
      _dbContext = new OublietteContext(new DbContextOptionsBuilder<OublietteContext>()
       .UseInMemoryDatabase(nameof(ResetPasswordTests))
       .Options);

      _dbContext.Database.EnsureDeleted();
      _dbContext.Database.EnsureCreated();

      _handler = new ResetPasswordHandler(
        _dateTime.Object,
        _dbContext,
        _password.Object,
        _token.Object
      );
    }

    [Fact]
    public async Task Given_NoMatchingUuid_When_Handle_Then_InvalidCredentialException()
    {
      string token = "token";
      _token.Setup(x => x.Validate(token)).Returns(new ClaimsPrincipal(new ClaimsIdentity(new[]
      {
        new Claim("sub", Guid.Empty.ToString())
      })));

      await Assert.ThrowsAsync<InvalidCredentialException>(async ()
        => await _handler.Handle(new ResetPassword(new ResetPasswordModel
        {
          Token = token
        }), default));
    }

    [Fact]
    public async Task Given_Request_When_Handle_Then_PasswordReset()
    {
      var passwordReset = new PasswordReset
      {
        Account = new Account(),
        Uuid = Guid.NewGuid()
      };
      _dbContext.PasswordResets.Add(passwordReset);
      _dbContext.SaveChanges();

      string token = passwordReset.Uuid.ToString();
      _token.Setup(x => x.Validate(token)).Returns(new ClaimsPrincipal(new ClaimsIdentity(new[]
      {
        new Claim("sub", passwordReset.Uuid.ToString())
      })));

      var passwordChanged = DateTime.Now;
      _dateTime.SetupGet(x => x.Now).Returns(passwordChanged);

      string passwordString = "Test123!";
      var password = new Password(passwordString, new byte[32], 10000);
      _password.Setup(x => x.Create(passwordString)).Returns(password);

      await _handler.Handle(new ResetPassword(new ResetPasswordModel
      {
        Password = passwordString,
        Token = token
      }), default);

      Assert.Empty(_dbContext.PasswordResets.ToArray());
      Assert.Equal(passwordChanged, passwordReset.Account.PasswordChanged);
      Assert.Same(password, passwordReset.Account.Password);
    }
  }
}
