﻿using AutoMapper;
using Moq;
using Oubliette.Api.Application.Mapping;
using Oubliette.Api.Application.Users.Models;
using Oubliette.Domain.Users;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Oubliette.Api.Application.Users.Queries
{
  [Trait(Traits.Category, Categories.Integration)]
  public class GetProfileTests
  {
    private readonly Mock<IApplicationContext> _appContext = new Mock<IApplicationContext>();

    private readonly GetProfileHandler _handler;

    public GetProfileTests()
    {
      var mapper = new Mapper(new MapperConfiguration(config =>
      {
        config.AddProfile(new AccountProfile());
      }));

      _handler = new GetProfileHandler(_appContext.Object, mapper);
    }

    [Fact]
    public async Task Given_Request_When_Handle_Then_ProfileModel()
    {
      var created = DateTime.Now;
      var account = new Account
      {
        Confirmed = created.AddMinutes(10),
        Created = created,
        Email = "email",
        Name = "name",
        PasswordChanged = created.AddHours(1),
        Updated = created.AddDays(1)
      };
      _appContext.SetupGet(x => x.Account).Returns(account);

      ProfileModel model = await _handler.Handle(new GetProfile(), default);

      Assert.Equal(account.Confirmed, model.Confirmed);
      Assert.Equal(account.Created, model.Created);
      Assert.Equal(account.Email, model.Email);
      Assert.Equal(account.Name, model.Name);
      Assert.Equal(account.PasswordChanged, model.PasswordChanged);
      Assert.Equal(account.Updated, model.Updated);
    }
  }
}
