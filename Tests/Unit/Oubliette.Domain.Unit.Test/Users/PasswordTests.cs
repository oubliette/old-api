﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using Xunit;

namespace Oubliette.Domain.Users
{
  [Trait(Traits.Category, Categories.Unit)]
  public class PasswordTests
  {
    private const int Iterations = 10000;
    private const string PasswordString = "Test123!";

    private readonly byte[] _bytes;
    private readonly byte[] _hash;
    private readonly byte[] _salt = new byte[32];

    public PasswordTests()
    {
      using var generator = new RNGCryptoServiceProvider();
      generator.GetBytes(_salt);

      using var pbkdf2 = new Rfc2898DeriveBytes(PasswordString, _salt, Iterations);
      _hash = pbkdf2.GetBytes(_salt.Length);

      var bytes = new List<byte>(capacity: 4 + 4 + _salt.Length + 4 + _hash.Length);

      bytes.AddRange(BitConverter.GetBytes(Iterations));
      bytes.AddRange(BitConverter.GetBytes(_salt.Length));
      bytes.AddRange(_salt);
      bytes.AddRange(BitConverter.GetBytes(_hash.Length));
      bytes.AddRange(_hash);

      _bytes = bytes.ToArray();
    }

    [Fact]
    public void Given_Arguments_When_ctor_Then_Password()
    {
      int hashLength = _salt.Length * 2;

      var password = new Password(PasswordString, _salt, Iterations, hashLength);

      using var pbkdf2 = new Rfc2898DeriveBytes(PasswordString, _salt, Iterations);
      byte[] hash = pbkdf2.GetBytes(hashLength);

      AssertEqual(password, hash);
    }

    [Fact]
    public void Given_ByteArray_When_ctor_Then_Password()
    {
      AssertEqual(new Password(_bytes));
    }

    [Fact]
    public void Given_DefaultArguments_When_ctor_Then_DefaultValues()
    {
      AssertEqual(new Password(PasswordString, _salt, Iterations));
    }

    [Fact]
    public void Given_InvalidPassword_When_Match_Then_False()
    {
      Assert.False(new Password(PasswordString, _salt, Iterations).Match(PasswordString + "*"));
    }

    [Fact]
    public void Given_NullPassword_When_ctor_Then_ArgumentNullException()
    {
      Assert.Equal("password", Assert.Throws<ArgumentNullException>(()
        => new Password(null, Array.Empty<byte>(), default)).ParamName);
    }

    [Fact]
    public void Given_NullSalt_When_ctor_Then_ArgumentNullException()
    {
      Assert.Equal("salt", Assert.Throws<ArgumentNullException>(()
        => new Password(string.Empty, null, default)).ParamName);
    }

    [Fact]
    public void Given_Password_When_ToByteArray_Then_ByteArray()
    {
      var password = new Password(PasswordString, _salt, Iterations);
      Assert.True(_bytes.SequenceEqual(password.ToByteArray()));
    }

    [Fact]
    public void Given_ValidPassword_When_Match_Then_True()
    {
      Assert.True(new Password(PasswordString, _salt, Iterations).Match(PasswordString));
    }

    private void AssertEqual(Password password, byte[] hash = null)
    {
      Assert.Equal(Iterations, (int)typeof(Password)
        .GetField("_iterations", BindingFlags.Instance | BindingFlags.NonPublic)
        .GetValue(password));
      Assert.True(_salt.SequenceEqual((byte[])typeof(Password)
        .GetField("_salt", BindingFlags.Instance | BindingFlags.NonPublic)
        .GetValue(password)));
      Assert.True((hash ?? _hash).SequenceEqual((byte[])typeof(Password)
        .GetField("_hash", BindingFlags.Instance | BindingFlags.NonPublic)
        .GetValue(password)));
    }
  }
}
