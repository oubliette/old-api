﻿using Microsoft.Extensions.Options;
using Moq;
using Oubliette.Api.Application.Users.Models;
using Oubliette.Domain.Users;
using System;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using Xunit;

namespace Oubliette.Api.Application.Users
{
  [Trait(Traits.Category, Categories.Unit)]
  public class AuthenticationServiceTests
  {
    private readonly Mock<IPassword> _password = new Mock<IPassword>();
    private readonly Mock<IToken> _token = new Mock<IToken>();
    private readonly TokenOptions _tokenOptions = new TokenOptions
    {
      Lifetime = 1000,
      Type = "type"
    };

    private readonly AuthenticationService _service;

    public AuthenticationServiceTests()
    {
      _service = new AuthenticationService(
        _password.Object,
        _token.Object,
        Options.Create(_tokenOptions)
      );
    }

    [Fact]
    public void Given_NullAccount_When_CreateToken_Then_ArgumentNullException()
    {
      Assert.Equal("session", Assert.Throws<ArgumentException>(()
        => _service.CreateToken(new Session())).ParamName);
    }

    [Fact]
    public void Given_NullSession_When_CreateToken_Then_ArgumentNullException()
    {
      Assert.Equal("session", Assert.Throws<ArgumentNullException>(()
        => _service.CreateToken(null)).ParamName);
    }

    [Fact]
    public void Given_Account_When_CreateToken_Then_TokenModel()
    {
      var session = new Session
      {
        Account = new Account
        {
          Uuid = Guid.NewGuid()
        },
        Uuid = Guid.NewGuid()
      };

      string accessToken = session.Account.Uuid.ToString("N");
      _token.Setup(x => x.Create(It.Is<ClaimsIdentity>(y
        => y.Claims.Single(x => x.Type == "sub").Value == session.Account.Uuid.ToString()
      ), _tokenOptions.Lifetime)).Returns(accessToken);

      var keyBytes = new byte[32];
      using var generator = new RNGCryptoServiceProvider();
      generator.GetBytes(keyBytes);
      string refreshToken = Convert.ToBase64String(session.Uuid.ToByteArray().Concat(keyBytes).ToArray());

      var key = new Password(Convert.ToBase64String(keyBytes), new byte[32], 10000);
      _password.Setup(x => x.Generate(keyBytes.Length, out keyBytes)).Returns(key);

      TokenModel token = _service.CreateToken(session);
      Assert.NotNull(accessToken);

      Assert.Same(key, session.Key);

      Assert.Equal(accessToken, token.AccessToken);
      Assert.Equal(_tokenOptions.Lifetime, token.ExpiresIn);
      Assert.Equal(refreshToken, token.RefreshToken);
      Assert.Equal(_tokenOptions.Type, token.TokenType);
    }
  }
}
