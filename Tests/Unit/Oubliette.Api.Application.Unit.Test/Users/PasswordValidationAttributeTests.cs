﻿using Xunit;

namespace Oubliette.Api.Application.Users
{
  [Trait(Traits.Category, Categories.Unit)]
  public class PasswordValidationAttributeTests
  {
    private readonly PasswordValidationAttribute _attribute = new PasswordValidationAttribute();

    [Fact]
    public void Given_Invalid_When_IsValid_Then_False()
    {
      Assert.False(_attribute.IsValid("TEST123!"));
      Assert.False(_attribute.IsValid("test123!"));
      Assert.False(_attribute.IsValid("Test1234"));
      Assert.False(_attribute.IsValid("Test!\"/$"));
    }

    [Fact]
    public void Given_NotString_When_IsValid_Then_False()
    {
      Assert.False(_attribute.IsValid(0));
    }

    [Fact]
    public void Given_Null_When_IsValid_Then_False()
    {
      Assert.False(_attribute.IsValid(null));
    }

    [Fact]
    public void Given_Valid_When_IsValid_Then_True()
    {
      Assert.True(_attribute.IsValid("Test123!"));
    }
  }
}
