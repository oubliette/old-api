﻿using System;
using System.Linq;
using System.Security.Cryptography;
using Xunit;

namespace Oubliette.Api.Application.Users
{
  [Trait(Traits.Category, Categories.Unit)]
  public class RefreshTokenTests
  {
    private readonly Guid _id = Guid.NewGuid();
    private readonly byte[] _key = new byte[32];
    private readonly string _value;

    public RefreshTokenTests()
    {
      using var generator = new RNGCryptoServiceProvider();
      generator.GetBytes(_key);

      _value = Convert.ToBase64String(_id.ToByteArray().Concat(_key).ToArray());
    }

    [Fact]
    public void Given_Arguments_When_ctor_Then_RefreshToken()
    {
      AssertEqual(new RefreshToken(_id, _key));
    }

    [Fact]
    public void Given_NullKey_When_ctor_Then_ArgumentNullException()
    {
      Assert.Equal("key", Assert.Throws<ArgumentNullException>(()
        => new RefreshToken(default, null)).ParamName);
    }

    [Fact]
    public void Given_RefreshToken_When_ToString_Then_Value()
    {
      Assert.Equal(_value, new RefreshToken(_id, _key).ToString());
    }

    [Fact]
    public void Given_Value_When_Parse_Then_RefreshToken()
    {
      AssertEqual(RefreshToken.Parse(_value));
    }

    private void AssertEqual(RefreshToken refreshToken)
    {
      Assert.Equal(_id, refreshToken.Id);
      Assert.Equal(_key, refreshToken.Key);
    }
  }
}
