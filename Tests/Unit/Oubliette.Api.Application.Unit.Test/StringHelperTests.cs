﻿using Xunit;

namespace Oubliette.Api.Application
{
  [Trait(Traits.Category, Categories.Unit)]
  public class StringHelperTests
  {
    [Fact]
    public void Given_NullOrWhiteSpaceValue_When_CleanTrim_Then_Null()
    {
      Assert.Null(StringHelper.CleanTrim(null));
      Assert.Null(StringHelper.CleanTrim(string.Empty));
      Assert.Null(StringHelper.CleanTrim("   "));
    }

    [Fact]
    public void Given_Value_When_CleanTrim_Then_Trimmed()
    {
      Assert.Equal("test", StringHelper.CleanTrim(" test   "));
    }
  }
}
