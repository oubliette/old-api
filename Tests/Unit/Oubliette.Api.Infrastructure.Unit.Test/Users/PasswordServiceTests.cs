﻿using Oubliette.Domain.Users;
using System;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using Xunit;

namespace Oubliette.Api.Infrastructure.Users
{
  [Trait(Traits.Category, Categories.Unit)]
  public class PasswordServiceTests
  {
    private const int Iterations = 31623;
    private const int SaltLength = 101;
    private const int HashLength = 180;

    private readonly PasswordService _service = new PasswordService(Iterations, SaltLength, HashLength);

    [Theory]
    [InlineData("Test123!")]
    public void Given_Password_When_Create_Then_Password(string passwordString)
    {
      Password password = _service.Create(passwordString);

      var hash = (byte[])typeof(Password)
        .GetField("_hash", BindingFlags.Instance | BindingFlags.NonPublic)
        .GetValue(password);
      Assert.Equal(HashLength, hash.Length);

      int iterations = (int)typeof(Password)
        .GetField("_iterations", BindingFlags.Instance | BindingFlags.NonPublic)
        .GetValue(password);
      Assert.Equal(Iterations, iterations);

      var salt = (byte[])typeof(Password)
        .GetField("_salt", BindingFlags.Instance | BindingFlags.NonPublic)
        .GetValue(password);
      Assert.Equal(SaltLength, salt.Length);

      using var pbkdf2 = new Rfc2898DeriveBytes(passwordString, salt, iterations);
      byte[] newHash = pbkdf2.GetBytes(HashLength);

      Assert.True(hash.SequenceEqual(newHash));
    }

    [Theory]
    [InlineData(20)]
    public void Given_Length_When_Generate_Then_Password(int length)
    {
      Password password = _service.Generate(length, out byte[] bytes);

      Assert.Equal(length, bytes.Length);
      Assert.True(password.Match(Convert.ToBase64String(bytes)));
    }
  }
}
