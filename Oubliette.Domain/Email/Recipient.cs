﻿#nullable enable
using System;

namespace Oubliette.Domain.Email
{
  public class Recipient
  {
    public Recipient(string address, string? displayName = null, RecipientType type = RecipientType.To)
    {
      Address = address ?? throw new ArgumentNullException(nameof(address));
      DisplayName = displayName;
      Type = type;
    }

    public string Address { get; }
    public string? DisplayName { get; }
    public RecipientType Type { get; }
  }
}
