﻿namespace Oubliette.Domain.Email
{
  public enum RecipientType
  {
    To = 0,
    CC = 1,
    Bcc = 2
  }
}
