﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;

namespace Oubliette.Domain.Email
{
  public class Message
  {
    public Message(string body, IEnumerable<Recipient> recipients, string subject, bool isBodyHtml = false)
    {
      Body = body ?? throw new ArgumentNullException(nameof(body));

      Recipients = recipients ?? throw new ArgumentNullException(nameof(recipients));
      if (recipients.Any(x => x == null))
      {
        throw new ArgumentException("The collection cannot contain a null element.", nameof(recipients));
      }
      else if (!recipients.Any(x => x.Type == RecipientType.To))
      {
        throw new ArgumentException($"The collection must contain at least one {RecipientType.To} element.", nameof(recipients));
      }

      Subject = subject ?? throw new ArgumentNullException(nameof(subject));
      IsBodyHtml = isBodyHtml;
    }

    public string Body { get; }
    public bool IsBodyHtml { get; }
    public IEnumerable<Recipient> Recipients { get; }
    public string Subject { get; }
  }
}
