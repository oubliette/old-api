﻿#nullable enable
using System;
using System.Collections.Generic;

namespace Oubliette.Domain.Users
{
  public class Account
  {
    public Account()
    {
      Email = string.Empty;
      Name = string.Empty;
      Password = new Password(Array.Empty<byte>());
      Username = string.Empty;
    }

    public DateTime? Confirmed { get; set; }
    public DateTime Created { get; set; }
    public string Email { get; set; }
    public int Id { get; set; }
    public string Name { get; set; }
    public Password Password { get; set; }
    public byte[] PasswordBytes
    {
      get => Password.ToByteArray();
      set => Password = new Password(value);
    }
    public DateTime? PasswordChanged { get; set; }
    public PasswordReset? PasswordReset { get; set; }
    public ICollection<Session>? Sessions { get; set; }
    public DateTime? Updated { get; set; }
    public string Username { get; set; }
    public Guid Uuid { get; set; }
  }
}
