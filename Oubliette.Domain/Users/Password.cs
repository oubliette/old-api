﻿#nullable enable
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

namespace Oubliette.Domain.Users
{
  public class Password
  {
    private readonly byte[] _hash;
    private readonly int _iterations;
    private readonly byte[] _salt;

    public Password(string password, byte[] salt, int iterations, int? hashLength = null)
    {
      if (password == null)
      {
        throw new ArgumentNullException(nameof(password));
      }

      _salt = salt ?? throw new ArgumentNullException(nameof(salt));
      _iterations = iterations;

      _hash = ComputeHash(password, hashLength ?? salt.Length);
    }
    public Password(byte[] bytes)
    {
      using var stream = new MemoryStream(bytes);
      var buffer = new byte[4];
      stream.Read(buffer, 0, buffer.Length);
      _iterations = BitConverter.ToInt32(buffer);

      buffer = new byte[4];
      stream.Read(buffer, 0, buffer.Length);
      _salt = new byte[BitConverter.ToInt32(buffer)];
      stream.Read(_salt, 0, _salt.Length);

      buffer = new byte[4];
      stream.Read(buffer, 0, buffer.Length);
      _hash = new byte[BitConverter.ToInt32(buffer)];
      stream.Read(_hash, 0, _hash.Length);
    }

    public bool Match(string password)
    {
      return _hash.SequenceEqual(ComputeHash(password, _hash.Length));
    }

    public byte[] ToByteArray()
    {
      var bytes = new List<byte>(capacity: 4 + 4 + _salt.Length + 4 +_hash.Length);

      bytes.AddRange(BitConverter.GetBytes(_iterations));
      bytes.AddRange(BitConverter.GetBytes(_salt.Length));
      bytes.AddRange(_salt);
      bytes.AddRange(BitConverter.GetBytes(_hash.Length));
      bytes.AddRange(_hash);

      return bytes.ToArray();
    }

    private byte[] ComputeHash(string password, int length)
    {
      using var pbkdf2 = new Rfc2898DeriveBytes(password, _salt, _iterations);

      return pbkdf2.GetBytes(length);
    }
  }
}
