﻿#nullable enable
using System;

namespace Oubliette.Domain.Users
{
  public class Session
  {
    public Session()
    {
      Key = new Password(Array.Empty<byte>());
    }

    public Account? Account { get; set; }
    public int AccountId { get; set; }
    public DateTime Created { get; set; }
    public int Id { get; set; }
    public Password Key { get; set; }
    public byte[] KeyBytes
    {
      get => Key.ToByteArray();
      set => Key = new Password(value);
    }
    public DateTime? Updated { get; set; }
    public Guid Uuid { get; set; }
  }
}
