﻿#nullable enable
using System;

namespace Oubliette.Domain.Users
{
  public class PasswordReset
  {
    public Account? Account { get; set; }
    public int AccountId { get; set; }
    public DateTime Created { get; set; }
    public DateTime? Updated { get; set; }
    public Guid Uuid { get; set; }
  }
}
