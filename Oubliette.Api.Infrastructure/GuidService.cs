﻿using Oubliette.Api.Application;
using System;

namespace Oubliette.Api.Infrastructure
{
  public class GuidService : IGuid
  {
    public Guid NewGuid()
    {
      return Guid.NewGuid();
    }
  }
}
