﻿using Microsoft.Extensions.DependencyInjection;
using Oubliette.Api.Application;
using Oubliette.Api.Application.Users;
using Oubliette.Api.Infrastructure.Mailgun;
using Oubliette.Api.Infrastructure.Users;

namespace Oubliette.Api.Infrastructure
{
  public static class DependencyInjection
  {
    public static IServiceCollection AddInfrastructure(this IServiceCollection services)
    {
      return services
        .AddSingleton<IDateTime, DateTimeService>()
        .AddSingleton<IEmail, EmailService>()
        .AddSingleton<IGuid, GuidService>()
        .AddSingleton<IPassword, PasswordService>()
        .AddSingleton<IToken, TokenService>();
    }
  }
}
