﻿using Oubliette.Api.Application;
using System;

namespace Oubliette.Api.Infrastructure
{
  public class DateTimeService : IDateTime
  {
    public DateTime Now => DateTime.UtcNow;
  }
}
