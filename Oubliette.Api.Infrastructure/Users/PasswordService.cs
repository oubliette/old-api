﻿#nullable enable
using Oubliette.Api.Application.Users;
using Oubliette.Domain.Users;
using System;
using System.Security.Cryptography;

namespace Oubliette.Api.Infrastructure.Users
{
  public class PasswordService : IPassword
  {
    private readonly RandomNumberGenerator _generator;
    private readonly int? _hashLength;
    private readonly int _iterations;
    private readonly int _saltLength;

    public PasswordService(int iterations = 10000, int saltLength = 32, int? hashLength = null)
    {
      _generator = new RNGCryptoServiceProvider();
      _iterations = iterations;
      _saltLength = saltLength;
      _hashLength = hashLength;
    }

    public Password Create(string password)
    {
      var salt = new byte[_saltLength];
      _generator.GetBytes(salt);

      return new Password(password, salt, _iterations, _hashLength);
    }

    public Password Generate(int length, out byte[] password)
    {
      password = new byte[length];
      _generator.GetBytes(password);

      return Create(Convert.ToBase64String(password));
    }
  }
}
