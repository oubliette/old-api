﻿namespace Oubliette.Api.Infrastructure.Users
{
  public class JwtOptions
  {
    public string Secret { get; set; }
  }
}
