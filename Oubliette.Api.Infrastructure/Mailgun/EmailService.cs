﻿#nullable enable
using Microsoft.Extensions.Options;
using Oubliette.Api.Application;
using Oubliette.Domain.Email;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Oubliette.Api.Infrastructure.Mailgun
{
  public class EmailService : IEmail
  {
    private readonly RestClient _client;
    private readonly MailAddress _sender;

    public EmailService(IOptions<MailgunOptions> options)
    {
      if (options == null)
      {
        throw new ArgumentNullException(nameof(options));
      }

      _client = new RestClient(options.Value.Endpoint)
      {
        Authenticator = new HttpBasicAuthenticator("api", options.Value.ApiKey)
      };

      string[] sender = options.Value.Sender.Split('<', '>');

      _sender = new MailAddress(
        address: sender.Length > 1 ? sender[1].Trim() : sender[0].Trim(),
        displayName: sender.Length > 1 ? sender[0].Trim() : null
      );
    }

    public async Task SendAsync(Message message, CancellationToken cancellationToken)
    {
      if (message == null)
      {
        throw new ArgumentNullException(nameof(message));
      }

      var request = new RestRequest(Method.POST);

      request.AddParameter("from", _sender.ToString());
      request.AddParameter("subject", message.Subject);
      request.AddParameter(message.IsBodyHtml ? "html" : "text", message.Body);

      foreach (Recipient recipient in message.Recipients)
      {
        string type = recipient.Type.ToString().ToLowerInvariant();
        request.AddParameter(type, new MailAddress(recipient.Address, recipient.DisplayName).ToString());
      }

      IRestResponse response = await _client.ExecuteAsync(request, cancellationToken);
      if (!response.IsSuccessful)
      {
        var error = new StringBuilder(response.StatusCode.ToString());
        error.Append(" ");
        error.AppendLine(response.StatusDescription);
        error.AppendLine(response.Content);
        throw new InvalidOperationException(error.ToString(), response.ErrorException);
      }
    }
  }
}
