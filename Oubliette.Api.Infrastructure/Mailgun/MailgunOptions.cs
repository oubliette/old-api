﻿namespace Oubliette.Api.Infrastructure.Mailgun
{
  public class MailgunOptions
  {
    public string ApiKey { get; set; }
    public string Endpoint { get; set; }
    public string Sender { get; set; }
  }
}
