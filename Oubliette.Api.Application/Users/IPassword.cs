﻿using Oubliette.Domain.Users;

namespace Oubliette.Api.Application.Users
{
  public interface IPassword
  {
    Password Create(string password);
    Password Generate(int length, out byte[] password);
  }
}
