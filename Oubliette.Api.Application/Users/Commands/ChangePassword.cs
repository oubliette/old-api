﻿#nullable enable
using MediatR;
using Oubliette.Api.Application.Users.Models;
using System;

namespace Oubliette.Api.Application.Users.Commands
{
  public class ChangePassword : IRequest
  {
    public ChangePassword(ChangePasswordModel model)
    {
      Model = model ?? throw new ArgumentNullException(nameof(model));
    }

    public ChangePasswordModel Model { get; }
  }
}
