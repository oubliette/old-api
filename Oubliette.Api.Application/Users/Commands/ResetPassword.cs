﻿#nullable enable
using MediatR;
using Oubliette.Api.Application.Users.Models;
using System;

namespace Oubliette.Api.Application.Users.Commands
{
  public class ResetPassword : IRequest
  {
    public ResetPassword(ResetPasswordModel model)
    {
      Model = model ?? throw new ArgumentNullException(nameof(model));
    }

    public ResetPasswordModel Model { get; }
  }
}
