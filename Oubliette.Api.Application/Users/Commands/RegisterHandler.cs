﻿#nullable enable
using MediatR;
using Microsoft.Extensions.Options;
using Oubliette.Domain.Email;
using Oubliette.Domain.Users;
using System;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace Oubliette.Api.Application.Users.Commands
{
  public class RegisterHandler : IRequestHandler<Register>
  {
    private const int TokenLifetime = 864000; // 10 days

    private readonly IDateTime _dateTime;
    private readonly IOublietteContext _dbContext;
    private readonly IEmail _email;
    private readonly IGuid _guid;
    private readonly HttpOptions _httpOptions;
    private readonly IPassword _password;
    private readonly IToken _token;

    public RegisterHandler(
      IDateTime dateTime,
      IOublietteContext dbContext,
      IEmail email,
      IGuid guid,
      IOptions<HttpOptions> httpOptions,
      IPassword password,
      IToken token
    )
    {
      _dateTime = dateTime;
      _dbContext = dbContext;
      _email = email;
      _guid = guid;
      _httpOptions = httpOptions.Value;
      _password = password;
      _token = token;
    }

    public async Task<Unit> Handle(Register request, CancellationToken cancellationToken)
    {
      var account = new Account
      {
        Created = _dateTime.Now,
        Email = request.Model.Email,
        Name = request.Model.Name.CleanTrim() ?? throw new ArgumentException("The name is required.", nameof(request)),
        Password = _password.Create(request.Model.Password),
        Username = request.Model.Email.ToLowerInvariant(),
        Uuid = _guid.NewGuid()
      };

      _dbContext.Accounts.Add(account);
      await _dbContext.SaveChangesAsync(cancellationToken);

      var token = _token.Create(new ClaimsIdentity(new[]
      {
        new Claim("sub", account.Uuid.ToString())
      }), TokenLifetime);

      await _email.SendAsync(new Message(
        subject: Resources.Email.AccountActivation_Subject,
        body: Resources.Email.AccountActivation_Body
          .Replace("{domain}", _httpOptions.Domain)
          .Replace("{name}", account.Name)
          .Replace("{token}", token),
        recipients: new[] { new Recipient(account.Email, account.Name) }
      ), cancellationToken);

      return Unit.Value;
    }
  }
}
