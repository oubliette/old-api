﻿#nullable enable
using MediatR;
using Microsoft.EntityFrameworkCore;
using Oubliette.Api.Application.Exceptions;
using Oubliette.Domain.Users;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Oubliette.Api.Application.Users.Commands
{
  public class LogOutHandler : IRequestHandler<LogOut>
  {
    private readonly IOublietteContext _dbContext;

    public LogOutHandler(IOublietteContext dbContext)
    {
      _dbContext = dbContext;
    }

    public async Task<Unit> Handle(LogOut request, CancellationToken cancellationToken)
    {
      RefreshToken refreshToken;
      try
      {
        refreshToken = RefreshToken.Parse(request.Model.RefreshToken);
      }
      catch (Exception exception)
      {
        throw new InvalidCredentialException(exception);
      }

      Session session = await _dbContext.Sessions
        .SingleOrDefaultAsync(x => x.Uuid == refreshToken.Id, cancellationToken);

      if (session?.Key.Match(Convert.ToBase64String(refreshToken.Key)) != true)
      {
        throw new InvalidCredentialException();
      }

      _dbContext.Sessions.Remove(session);
      await _dbContext.SaveChangesAsync(cancellationToken);

      return Unit.Value;
    }
  }
}
