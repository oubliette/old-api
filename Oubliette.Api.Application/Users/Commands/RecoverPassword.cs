﻿#nullable enable
using MediatR;
using Oubliette.Api.Application.Users.Models;
using System;

namespace Oubliette.Api.Application.Users.Commands
{
  public class RecoverPassword : IRequest
  {
    public RecoverPassword(RecoverPasswordModel model)
    {
      Model = model ?? throw new ArgumentNullException(nameof(model));
    }

    public RecoverPasswordModel Model { get; }
  }
}
