﻿#nullable enable
using MediatR;
using Microsoft.EntityFrameworkCore;
using Oubliette.Api.Application.Exceptions;
using Oubliette.Domain.Users;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace Oubliette.Api.Application.Users.Commands
{
  public class ResetPasswordHandler : IRequestHandler<ResetPassword>
  {
    private readonly IDateTime _dateTime;
    private readonly IOublietteContext _dbContext;
    private readonly IPassword _password;
    private readonly IToken _token;

    public ResetPasswordHandler(
      IDateTime dateTime,
      IOublietteContext dbContext,
      IPassword password,
      IToken token
    )
    {
      _dateTime = dateTime;
      _dbContext = dbContext;
      _password = password;
      _token = token;
    }

    public async Task<Unit> Handle(ResetPassword request, CancellationToken cancellationToken)
    {
      ClaimsPrincipal principal;
      try
      {
        principal = _token.Validate(request.Model.Token);
      }
      catch (Exception exception)
      {
        throw new InvalidCredentialException(exception);
      }

      Claim claim = principal.Claims.Single(x => x.Type == "sub");
      var uuid = Guid.Parse(claim.Value);

      PasswordReset passwordReset = await _dbContext.PasswordResets
        .Include(x => x.Account)
        .SingleOrDefaultAsync(x => x.Uuid == uuid, cancellationToken);

      if (passwordReset == null)
      {
        throw new InvalidCredentialException();
      }
      else if (passwordReset.Account == null)
      {
        throw new InvalidOperationException("The Account cannot be null.");
      }

      _dbContext.PasswordResets.Remove(passwordReset);

      passwordReset.Account.Password = _password.Create(request.Model.Password);
      passwordReset.Account.PasswordChanged = _dateTime.Now;
      await _dbContext.SaveChangesAsync(cancellationToken);

      return Unit.Value;
    }
  }
}
