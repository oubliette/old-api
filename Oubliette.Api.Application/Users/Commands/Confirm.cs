﻿#nullable enable
using MediatR;
using Oubliette.Api.Application.Users.Models;
using System;

namespace Oubliette.Api.Application.Users.Commands
{
  public class Confirm : IRequest
  {
    public Confirm(ConfirmModel model)
    {
      Model = model ?? throw new ArgumentNullException(nameof(model));
    }

    public ConfirmModel Model { get; }
  }
}
