﻿#nullable enable
using MediatR;
using Oubliette.Api.Application.Exceptions;
using Oubliette.Domain.Users;
using System.Threading;
using System.Threading.Tasks;

namespace Oubliette.Api.Application.Users.Commands
{
  public class ChangePasswordHandler : IRequestHandler<ChangePassword>
  {
    private readonly IApplicationContext _appContext;
    private readonly IDateTime _dateTime;
    private readonly IOublietteContext _dbContext;
    private readonly IPassword _password;

    public ChangePasswordHandler(
      IApplicationContext appContext,
      IDateTime dateTime,
      IOublietteContext dbContext,
      IPassword password
    )
    {
      _appContext = appContext;
      _dateTime = dateTime;
      _dbContext = dbContext;
      _password = password;
    }

    public async Task<Unit> Handle(ChangePassword request, CancellationToken cancellationToken)
    {
      Account account = _appContext.Account;

      if (!_appContext.Account.Password.Match(request.Model.Current))
      {
        throw new InvalidCredentialException();
      }

      account.Password = _password.Create(request.Model.Password);
      account.PasswordChanged = _dateTime.Now;
      await _dbContext.SaveChangesAsync(cancellationToken);

      return Unit.Value;
    }
  }
}
