﻿#nullable enable
using MediatR;
using Oubliette.Api.Application.Users.Models;
using System;

namespace Oubliette.Api.Application.Users.Commands
{
  public class Register : IRequest
  {
    public Register(RegisterModel model)
    {
      Model = model ?? throw new ArgumentNullException(nameof(model));
    }

    public RegisterModel Model { get; }
  }
}
