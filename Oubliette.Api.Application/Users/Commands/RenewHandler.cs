﻿#nullable enable
using MediatR;
using Microsoft.EntityFrameworkCore;
using Oubliette.Api.Application.Exceptions;
using Oubliette.Api.Application.Users.Models;
using Oubliette.Domain.Users;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Oubliette.Api.Application.Users.Commands
{
  public class RenewHandler : IRequestHandler<Renew, TokenModel>
  {
    private readonly IAuthentication _authentication;
    private readonly IDateTime _dateTime;
    private readonly IOublietteContext _dbContext;

    public RenewHandler(
      IAuthentication authentication,
      IDateTime dateTime,
      IOublietteContext dbContext
    )
    {
      _authentication = authentication;
      _dateTime = dateTime;
      _dbContext = dbContext;
    }

    public async Task<TokenModel> Handle(Renew request, CancellationToken cancellationToken)
    {
      RefreshToken refreshToken;
      try
      {
        refreshToken = RefreshToken.Parse(request.Model.RefreshToken);
      }
      catch (Exception exception)
      {
        throw new InvalidCredentialException(exception);
      }

      Session session = await _dbContext.Sessions
        .Include(x => x.Account)
        .SingleOrDefaultAsync(x => x.Uuid == refreshToken.Id, cancellationToken);

      if (session?.Key.Match(Convert.ToBase64String(refreshToken.Key)) != true)
      {
        throw new InvalidCredentialException();
      }

      TokenModel token = _authentication.CreateToken(session);

      session.Updated = _dateTime.Now;
      await _dbContext.SaveChangesAsync(cancellationToken);

      return token;
    }
  }
}
