﻿#nullable enable
using MediatR;
using Microsoft.EntityFrameworkCore;
using Oubliette.Api.Application.Exceptions;
using Oubliette.Api.Application.Users.Models;
using Oubliette.Domain.Users;
using System.Threading;
using System.Threading.Tasks;

namespace Oubliette.Api.Application.Users.Commands
{
  public class LogInHandler : IRequestHandler<LogIn, TokenModel>
  {
    private readonly IAuthentication _authentication;
    private readonly IDateTime _dateTime;
    private readonly IOublietteContext _dbContext;
    private readonly IGuid _guid;

    public LogInHandler(
      IAuthentication authentication,
      IDateTime dateTime,
      IOublietteContext dbContext,
      IGuid guid
    )
    {
      _authentication = authentication;
      _dateTime = dateTime;
      _dbContext = dbContext;
      _guid = guid;
    }

    public async Task<TokenModel> Handle(LogIn request, CancellationToken cancellationToken)
    {
      Account account = await _dbContext.Accounts.SingleOrDefaultAsync(
        x => x.Username == request.Model.Username.ToLowerInvariant(),
        cancellationToken
      );

      if (account?.Password.Match(request.Model.Password) != true)
      {
        throw new InvalidCredentialException();
      }
      else if (account?.Confirmed == null)
      {
        throw new NotConfirmedException();
      }

      var session = new Session
      {
        Account = account,
        AccountId = account.Id,
        Created = _dateTime.Now,
        Uuid = _guid.NewGuid()
      };
      _dbContext.Sessions.Add(session);

      TokenModel token = _authentication.CreateToken(session);
      
      await _dbContext.SaveChangesAsync(cancellationToken);

      return token;
      
    }
  }
}
