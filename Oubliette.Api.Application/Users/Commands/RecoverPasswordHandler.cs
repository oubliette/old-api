﻿#nullable enable
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Oubliette.Domain.Email;
using Oubliette.Domain.Users;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace Oubliette.Api.Application.Users.Commands
{
  public class RecoverPasswordHandler : IRequestHandler<RecoverPassword>
  {
    private const int TokenLifetime = 86400; // 24 hours

    private readonly IDateTime _dateTime;
    private readonly IOublietteContext _dbContext;
    private readonly IEmail _email;
    private readonly IGuid _guid;
    private readonly HttpOptions _httpOptions;
    private readonly IToken _token;

    public RecoverPasswordHandler(
      IDateTime dateTime,
      IOublietteContext dbContext,
      IEmail email,
      IGuid guid,
      IOptions<HttpOptions> httpOptions,
      IToken token
    )
    {
      _dateTime = dateTime;
      _dbContext = dbContext;
      _email = email;
      _guid = guid;
      _httpOptions = httpOptions.Value;
      _token = token;
    }

    public async Task<Unit> Handle(RecoverPassword request, CancellationToken cancellationToken)
    {
      Account account = await _dbContext.Accounts
        .Include(x => x.PasswordReset)
        .SingleOrDefaultAsync(
          x => x.Username == request.Model.Username.ToLowerInvariant(),
          cancellationToken
        );

      if (account != null)
      {
        if (account.PasswordReset == null)
        {
          account.PasswordReset = new PasswordReset
          {
            AccountId = account.Id,
            Created = _dateTime.Now
          };
          _dbContext.PasswordResets.Add(account.PasswordReset);
        }
        else
        {
          account.PasswordReset.Updated = _dateTime.Now;
        }

        account.PasswordReset.Uuid = _guid.NewGuid();
        await _dbContext.SaveChangesAsync(cancellationToken);

        string token = _token.Create(new ClaimsIdentity(new[]
        {
          new Claim("sub", account.PasswordReset.Uuid.ToString())
        }), TokenLifetime);

        await _email.SendAsync(new Message(
          subject: Resources.Email.PasswordRecovery_Subject,
          body: Resources.Email.PasswordRecovery_Body
            .Replace("{domain}", _httpOptions.Domain)
            .Replace("{name}", account.Name)
            .Replace("{token}", token),
          recipients: new[] { new Recipient(account.Email, account.Name) }
        ), cancellationToken);
      }

      return Unit.Value;
    }
  }
}
