﻿#nullable enable
using MediatR;
using Microsoft.EntityFrameworkCore;
using Oubliette.Api.Application.Exceptions;
using Oubliette.Domain.Users;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace Oubliette.Api.Application.Users.Commands
{
  public class ConfirmHandler : IRequestHandler<Confirm>
  {
    private readonly IDateTime _dateTime;
    private readonly IOublietteContext _dbContext;
    private readonly IToken _token;

    public ConfirmHandler(IDateTime dateTime, IOublietteContext dbContext, IToken token)
    {
      _dateTime = dateTime;
      _dbContext = dbContext;
      _token = token;
    }

    public async Task<Unit> Handle(Confirm request, CancellationToken cancellationToken)
    {
      ClaimsPrincipal principal;
      try
      {
        principal = _token.Validate(request.Model.Token);
      }
      catch (Exception exception)
      {
        throw new InvalidCredentialException(exception);
      }

      Claim claim = principal.Claims.Single(x => x.Type == "sub");
      var uuid = Guid.Parse(claim.Value);

      Account account = await _dbContext.Accounts.SingleAsync(x => x.Uuid == uuid, cancellationToken);
      if (!account.Confirmed.HasValue)
      {
        account.Confirmed = _dateTime.Now;
        await _dbContext.SaveChangesAsync(cancellationToken);
      }

      return Unit.Value;
    }
  }
}
