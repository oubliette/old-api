﻿#nullable enable
using System;
using System.Linq;

namespace Oubliette.Api.Application.Users
{
  public class RefreshToken
  {
    private const int GuidByteCount = 16;

    public RefreshToken(Guid id, byte[] key)
    {
      Id = id;
      Key = key ?? throw new ArgumentNullException(nameof(key));
    }

    public Guid Id { get; }
    public byte[] Key { get; }

    public static RefreshToken Parse(string s)
    {
      byte[] bytes = Convert.FromBase64String(s);

      return new RefreshToken(
        new Guid(bytes.Take(GuidByteCount).ToArray()),
        bytes.Skip(GuidByteCount).ToArray()
      );
    }

    public override string ToString()
    {
      return Convert.ToBase64String(Id.ToByteArray().Concat(Key).ToArray());
    }
  }
}
