﻿using System.Security.Claims;

namespace Oubliette.Api.Application.Users
{
  public interface IToken
  {
    string Create(ClaimsIdentity subject, int? lifetime = null);
    ClaimsPrincipal Validate(string token);
  }
}
