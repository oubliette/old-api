﻿using Oubliette.Api.Application.Users.Models;
using Oubliette.Domain.Users;

namespace Oubliette.Api.Application.Users
{
  public interface IAuthentication
  {
    TokenModel CreateToken(Session session);
  }
}
