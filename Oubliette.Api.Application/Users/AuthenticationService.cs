﻿#nullable enable
using Microsoft.Extensions.Options;
using Oubliette.Api.Application.Users.Models;
using Oubliette.Domain.Users;
using System;
using System.Security.Claims;

namespace Oubliette.Api.Application.Users
{
  public class AuthenticationService : IAuthentication
  {
    private const int SessionKeyLength = 32;

    private readonly IPassword _password;
    private readonly IToken _token;
    private readonly TokenOptions _tokenOptions;

    public AuthenticationService(IPassword password, IToken token, IOptions<TokenOptions> tokenOptions)
    {
      _password = password;
      _token = token;
      _tokenOptions = tokenOptions.Value;
    }

    public TokenModel CreateToken(Session session)
    {
      if (session == null)
      {
        throw new ArgumentNullException(nameof(session));
      }
      else if (session.Account == null)
      {
        throw new ArgumentException("The Account cannot be null.", nameof(session));
      }

      session.Key = _password.Generate(SessionKeyLength, out byte[] key);

      string accessToken = _token.Create(new ClaimsIdentity(new[]
      {
        new Claim("sub", session.Account.Uuid.ToString())
      }), _tokenOptions.Lifetime);

      return new TokenModel
      {
        AccessToken = accessToken,
        ExpiresIn = _tokenOptions.Lifetime,
        RefreshToken = new RefreshToken(session.Uuid, key).ToString(),
        TokenType = _tokenOptions.Type
      };
    }
  }
}
