﻿using System.ComponentModel.DataAnnotations;

namespace Oubliette.Api.Application.Users.Models
{
  public class ChangePasswordModel
  {
    [Required]
    public string Current { get; set; }

    [Required]
    [MinLength(8)]
    [PasswordValidation]
    public string Password { get; set; }
  }
}
