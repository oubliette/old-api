﻿using System.ComponentModel.DataAnnotations;

namespace Oubliette.Api.Application.Users.Models
{
  public class RecoverPasswordModel
  {
    [Required]
    public string Username { get; set; }
  }
}
