﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Oubliette.Api.Application.Users.Models
{
  public class RenewModel
  {
    [Required]
    [JsonPropertyName("refresh_token")]
    public string RefreshToken { get; set; }
  }
}
