﻿using System;

namespace Oubliette.Api.Application.Users.Models
{
  public class ProfileModel
  {
    public DateTime? Confirmed { get; set; }
    public DateTime Created { get; set; }
    public string Email { get; set; }
    public string Name { get; set; }
    public DateTime? PasswordChanged { get; set; }
    public DateTime? Updated { get; set; }
  }
}
