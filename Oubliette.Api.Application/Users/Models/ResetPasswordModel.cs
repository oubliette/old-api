﻿using System.ComponentModel.DataAnnotations;

namespace Oubliette.Api.Application.Users.Models
{
  public class ResetPasswordModel
  {
    [Required]
    [MinLength(8)]
    [PasswordValidation]
    public string Password { get; set; }

    [Required]
    public string Token { get; set; }
  }
}
