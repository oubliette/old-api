﻿using System.ComponentModel.DataAnnotations;

namespace Oubliette.Api.Application.Users.Models
{
  public class ConfirmModel
  {
    [Required]
    public string Token { get; set; }
  }
}
