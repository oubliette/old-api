﻿using System.ComponentModel.DataAnnotations;

namespace Oubliette.Api.Application.Users.Models
{
  public class RegisterModel
  {
    [Required]
    [MaxLength(255)]
    [EmailAddress]
    public string Email { get; set; }
    
    [Required]
    [MaxLength(255)]
    public string Name { get; set; }
    
    [Required]
    [MinLength(8)]
    [PasswordValidation]
    public string Password { get; set; }
  }
}
