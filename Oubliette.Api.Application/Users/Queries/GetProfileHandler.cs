﻿#nullable enable
using AutoMapper;
using MediatR;
using Oubliette.Api.Application.Users.Models;
using System.Threading;
using System.Threading.Tasks;

namespace Oubliette.Api.Application.Users.Queries
{
  public class GetProfileHandler : IRequestHandler<GetProfile, ProfileModel>
  {
    private readonly IApplicationContext _appContext;
    private readonly IMapper _mapper;

    public GetProfileHandler(IApplicationContext appContext, IMapper mapper)
    {
      _appContext = appContext;
      _mapper = mapper;
    }

    public Task<ProfileModel> Handle(GetProfile request, CancellationToken cancellationToken)
    {
      return Task.FromResult(_mapper.Map<ProfileModel>(_appContext.Account));
    }
  }
}
