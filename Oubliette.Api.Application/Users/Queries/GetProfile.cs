﻿#nullable enable
using MediatR;
using Oubliette.Api.Application.Users.Models;

namespace Oubliette.Api.Application.Users.Queries
{
  public class GetProfile : IRequest<ProfileModel>
  {
  }
}
