﻿namespace Oubliette.Api.Application.Users
{
  public class TokenOptions
  {
    public int Lifetime { get; set; }
    public string Type { get; set; }
  }
}
