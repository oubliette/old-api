﻿using System.ComponentModel.DataAnnotations;

namespace Oubliette.Api.Application.Users
{
  public class PasswordValidationAttribute : ValidationAttribute
  {
    public override string FormatErrorMessage(string name)
    {
      return $"The {name} must contain at least 1 uppercase letter, 1 lowercase letter, 1 digit and 1 special character.";
    }

    public override bool IsValid(object value)
    {
      if (value is string password)
      {
        int digitCount = 0, lowerCount = 0, otherCount = 0, upperCount = 0;

        foreach (char current in password)
        {
          if (char.IsLetter(current))
          {
            if (char.IsLower(current))
            {
              lowerCount++;
            }
            else
            {
              upperCount++;
            }
          }
          else if (char.IsDigit(current))
          {
            digitCount++;
          }
          else
          {
            otherCount++;
          }
        }

        return digitCount > 0 && lowerCount > 0 && otherCount > 0 && upperCount > 0;
      }

      return false;
    }
  }
}
