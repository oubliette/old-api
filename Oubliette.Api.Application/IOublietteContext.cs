﻿using Microsoft.EntityFrameworkCore;
using Oubliette.Domain.Users;
using System.Threading;
using System.Threading.Tasks;

namespace Oubliette.Api.Application
{
  public interface IOublietteContext
  {
    DbSet<Account> Accounts { get; }
    DbSet<PasswordReset> PasswordResets { get; }
    DbSet<Session> Sessions { get; }

    Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
  }
}
