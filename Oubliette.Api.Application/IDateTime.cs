﻿using System;

namespace Oubliette.Api.Application
{
  public interface IDateTime
  {
    DateTime Now { get; }
  }
}
