﻿using System;

namespace Oubliette.Api.Application
{
  public interface IGuid
  {
    Guid NewGuid();
  }
}
