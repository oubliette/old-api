﻿using Oubliette.Domain.Users;

namespace Oubliette.Api.Application
{
  public interface IApplicationContext
  {
    Account Account { get; }
  }
}
