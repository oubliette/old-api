﻿using AutoMapper;
using Oubliette.Api.Application.Users.Models;
using Oubliette.Domain.Users;

namespace Oubliette.Api.Application.Mapping
{
  public class AccountProfile : Profile
  {
    public AccountProfile()
    {
      CreateMap<Account, ProfileModel>();
    }
  }
}
