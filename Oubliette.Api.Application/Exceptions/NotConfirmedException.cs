﻿namespace Oubliette.Api.Application.Exceptions
{
  public class NotConfirmedException : BadRequestException
  {
    public NotConfirmedException()
    {
      Value = new { code = "not_confirmed" };
    }
  }
}
