﻿using System;
using System.Net;

namespace Oubliette.Api.Application.Exceptions
{
  public class BadRequestException : ApiException
  {
    public BadRequestException() : base(HttpStatusCode.BadRequest)
    {
    }
    public BadRequestException(Exception innerException)
      : base(HttpStatusCode.BadRequest, innerException: innerException)
    {
    }
  }
}
