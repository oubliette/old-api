﻿using System;

namespace Oubliette.Api.Application.Exceptions
{
  public class InvalidCredentialException : BadRequestException
  {
    public InvalidCredentialException(Exception innerException = null) : base(innerException)
    {
      Value = new { code = "invalid_credentials" };
    }
  }
}
