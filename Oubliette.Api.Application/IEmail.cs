﻿using Oubliette.Domain.Email;
using System.Threading;
using System.Threading.Tasks;

namespace Oubliette.Api.Application
{
  public interface IEmail
  {
    Task SendAsync(Message message, CancellationToken cancellationToken);
  }
}
